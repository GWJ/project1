package net.suncaper.springbootdemo.dto;

import net.suncaper.springbootdemo.pojo.SysUser;

/**
 * 用户列表的数据传输对象
 */
public class SysUserDto extends SysUser {
    private String rolename;

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
