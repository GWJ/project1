package net.suncaper.springbootdemo.controllers;

import net.suncaper.springbootdemo.common.SimpMsg;
import net.suncaper.springbootdemo.dto.SysUserDto;
import net.suncaper.springbootdemo.pojo.SysRole;
import net.suncaper.springbootdemo.pojo.SysUser;
import net.suncaper.springbootdemo.services.interfaces.SysRoleService;
import net.suncaper.springbootdemo.services.interfaces.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 用户管理控制层
 */
@Controller
@RequestMapping("/sys/user")
public class SysUserController {
    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserService sysUserService;
    /**
     * 进入到增加页面
     * @param model
     * @return
     */
    @RequestMapping("/toadd")
    public String toadd(Model model){
        SysRole role = new SysRole();
        role.setRolestate(1);
        List<SysRole> rolelist = sysRoleService.selectRoleList(role);
        model.addAttribute("rolelist",rolelist);
        return "/sys/user/add";
    }

    /**
     * 检测用户名是否存在
     * @param username
     * @return
     */
    @RequestMapping("/checkuser")
    @ResponseBody
    public SimpMsg checkUserName(String username){
        SysUser user = sysUserService.selectUserByName(username);
        SimpMsg smsg = new SimpMsg();
        if(user!=null){
            smsg.setCode(1);
            smsg.setMsg("用户名已存在！");
        }else{
            smsg.setCode(0);
            smsg.setMsg("用户名不存在，可以使用！");
        }
        return smsg;
    }

    @RequestMapping("/list")
    @ResponseBody
    public List<SysUserDto> selectUserList(){
        return sysUserService.selectUserList(null);
    }
}
