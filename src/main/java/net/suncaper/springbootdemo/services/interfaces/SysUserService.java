package net.suncaper.springbootdemo.services.interfaces;

import net.suncaper.springbootdemo.dto.SysUserDto;
import net.suncaper.springbootdemo.pojo.SysUser;

import java.util.List;

/**
 * 用户管理service层接口
 */
public interface SysUserService {
    /**
     * 根据用户名查询用户信息（主要用于验证用户是否存在）
     * @param username
     * @return 返回null表示用户不存在
     */
    SysUser selectUserByName(String username);

    /**
     * 查询用户列表
     * @param user
     * @return
     */
    List<SysUserDto> selectUserList(SysUser user);
}
