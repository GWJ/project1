package net.suncaper.springbootdemo.services.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.suncaper.springbootdemo.dao.SysUserMapper;
import net.suncaper.springbootdemo.dto.SysUserDto;
import net.suncaper.springbootdemo.pojo.SysUser;
import net.suncaper.springbootdemo.services.interfaces.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户管理servcie层实现类
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper mapper;

    /**
     * 根据用户名查询用户信息（主要用于验证用户是否存在）
     *
     * @param username
     * @return 返回null表示用户不存在
     */
    @Override
    public SysUser selectUserByName(String username) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        List<SysUser> userlist = mapper.selectList(wrapper);
        if(userlist!=null && userlist.size()>0){
            return userlist.get(0);
        }else{
            return null;
        }
    }

    /**
     * 查询用户列表
     *
     * @param user
     * @return
     */
    @Override
    public List<SysUserDto> selectUserList(SysUser user) {
        return mapper.selectUserList(user);
    }
}
