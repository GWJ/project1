package net.suncaper.springbootdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.suncaper.springbootdemo.dto.SysUserDto;
import net.suncaper.springbootdemo.pojo.SysUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统用户管理dao层
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 查询用户列表
     * @param user
     * @return
     */
    @Select("select userid,username,usertruename,usersex,userstate,a.roleid,b.rolename " +
            " from sysuser a inner join sysrole b on a.roleid=b.roleid ")
    List<SysUserDto> selectUserList(SysUser user);
}