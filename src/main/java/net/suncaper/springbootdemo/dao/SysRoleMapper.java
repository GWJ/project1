package net.suncaper.springbootdemo.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.suncaper.springbootdemo.pojo.SysRole;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

}